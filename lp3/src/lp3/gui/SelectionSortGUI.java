package lp3.gui;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import lp3.algoritm.ordenac.SelectionSort;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;

public class SelectionSortGUI {

	private JTextPane textArranjoOrig;
	private JTextPane textElementosOrdenados;
	private JTextPane textNumInterac;
	
	public JFrame frmSelectionSort;
	private JTextField textElement;
	private SelectionSort ss;
	private List<Integer> array;

	public SelectionSortGUI() {
		initialize();
		this.ss = new SelectionSort();
		this.array = new ArrayList<Integer>();
	}

	private void initialize() {
		frmSelectionSort = new JFrame();
		frmSelectionSort.setTitle("Selection Sort");
		frmSelectionSort.setBounds(100, 100, 612, 393);
		frmSelectionSort.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSelectionSort.getContentPane().setLayout(null);
		
		JLabel lblElemento = new JLabel("Elemento:");
		lblElemento.setBounds(46, 34, 100, 15);
		frmSelectionSort.getContentPane().add(lblElemento);
		
		textElement = new JTextField();
		textElement.setBounds(153, 32, 114, 19);
		frmSelectionSort.getContentPane().add(textElement);
		textElement.setColumns(10);
		
		JButton btnAdd = new JButton("add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				array.add(Integer.parseInt(textElement.getText()));
				textElement.setText(null);
				textArranjoOrig.setText(array.toString());
			}
		});
		btnAdd.setBounds(324, 29, 117, 25);
		frmSelectionSort.getContentPane().add(btnAdd);
		
		JButton btnRemov = new JButton("remove");
		btnRemov.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(array.contains(Integer.parseInt(textElement.getText()))) {
					textElement.setText(null);
					textArranjoOrig.setText(array.toString());
				} else {
					JOptionPane.showMessageDialog(null, "Número não pertence ao arranjo!");
				}
			}
		});
		btnRemov.setBounds(463, 29, 117, 25);
		frmSelectionSort.getContentPane().add(btnRemov);
		
		JLabel lblAranjoOriginal = new JLabel("Aranjo Original:");
		lblAranjoOriginal.setBounds(23, 112, 135, 15);
		frmSelectionSort.getContentPane().add(lblAranjoOriginal);
		
		textArranjoOrig = new JTextPane();
		textArranjoOrig.setEditable(false);
		textArranjoOrig.setBounds(172, 112, 408, 21);
		frmSelectionSort.getContentPane().add(textArranjoOrig);
		
		JLabel lblArranjoOrdenado = new JLabel("Arranjo Ordenado:");
		lblArranjoOrdenado.setBounds(23, 200, 145, 15);
		frmSelectionSort.getContentPane().add(lblArranjoOrdenado);
		
		textElementosOrdenados = new JTextPane();
		textElementosOrdenados.setEditable(false);
		textElementosOrdenados.setBounds(173, 200, 408, 21);
		frmSelectionSort.getContentPane().add(textElementosOrdenados);
		
		JButton btnSort = new JButton("sort");
		btnSort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textElementosOrdenados.setText(ss.sort2(array).toString());
				textNumInterac.setText("" + ss.getNumInt());
			}
		});
		btnSort.setBounds(424, 281, 117, 25);
		frmSelectionSort.getContentPane().add(btnSort);
		
		JLabel lblNmeroDeInteraes = new JLabel("Número de Interações:");
		lblNmeroDeInteraes.setBounds(23, 286, 173, 15);
		frmSelectionSort.getContentPane().add(lblNmeroDeInteraes);
		
		textNumInterac = new JTextPane();
		textNumInterac.setEditable(false);
		textNumInterac.setBounds(214, 285, 63, 21);
		frmSelectionSort.getContentPane().add(textNumInterac);
		
		JButton btnReset = new JButton("reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				array = new ArrayList<Integer>();
				ss = new SelectionSort();
				textArranjoOrig.setText(null);
				textElement.setText(null);
				textElementosOrdenados.setText(null);
				textNumInterac.setText(null);
			}
		});
		btnReset.setBounds(424, 356, 117, 25);
		frmSelectionSort.getContentPane().add(btnReset);
	}
}
