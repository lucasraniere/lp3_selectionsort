package lp3.main;

import java.awt.EventQueue;
import lp3.gui.SelectionSortGUI;

public class Main {

	public static void main(String[] args) {
		
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SelectionSortGUI window = new SelectionSortGUI();
					window.frmSelectionSort.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
