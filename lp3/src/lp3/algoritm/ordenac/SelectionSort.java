package lp3.algoritm.ordenac;

import java.util.List;

public class SelectionSort {
	private int numInt;
	
	public SelectionSort() {
		this.numInt = 0;
	}
	
	public int[] sort(int[] elementos) {
		int menor, indiceMenor;
		for(int i = 0; i < elementos.length; i++) {
			menor = elementos[i];
			indiceMenor = i;
			for(int j = i+1; j < elementos.length; j++) {
				if(elementos[j] < menor) {
					menor = elementos[j];
					indiceMenor = j;
				}
				this.numInt++;
			}
			elementos[indiceMenor] = elementos[i];
			elementos[i] = menor;
			this.numInt++;
		}
		return elementos;
	}
	
	public List<Integer> sort2(List<Integer> elementos) {
		numInt = 0;
		int menor, indiceMenor;
		for(int i = 0; i < elementos.size(); i++) {
			menor = elementos.get(i);
			indiceMenor = i;
			for(int j = i+1; j < elementos.size(); j++) {
				if(elementos.get(j) < menor) {
					menor = elementos.get(j);
					indiceMenor = j;
				}
				this.numInt++;
			}
			elementos.set(indiceMenor, elementos.get(i));
			elementos.set(i, menor);
			this.numInt++;
		}
		return elementos;
	}
	
	public int getNumInt() {
		return numInt;
	}

	@Override
	public String toString() {
		return "\nNúmero de Interações (Loops): " + this.numInt;
	}
}
